package ru.t1.sukhorukova.tm.api;

public interface ITaskController {

    void createTask();

    void showTasks();

    void clearTasks();

}
