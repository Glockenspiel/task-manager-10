package ru.t1.sukhorukova.tm.api;

import ru.t1.sukhorukova.tm.model.Project;

public interface IProjectService extends IProjectRepository {

    Project create(String name, String description);

}
