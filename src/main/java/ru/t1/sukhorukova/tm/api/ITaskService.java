package ru.t1.sukhorukova.tm.api;

import ru.t1.sukhorukova.tm.model.Task;

public interface ITaskService extends ITaskRepository {

    Task create(String name, String description);

}
