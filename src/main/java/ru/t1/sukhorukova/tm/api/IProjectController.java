package ru.t1.sukhorukova.tm.api;

public interface IProjectController {

    void createProject();

    void showProjects();

    void clearProjects();

}
