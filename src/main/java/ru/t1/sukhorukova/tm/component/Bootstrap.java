package ru.t1.sukhorukova.tm.component;

import ru.t1.sukhorukova.tm.api.*;
import ru.t1.sukhorukova.tm.constant.ArgumentConst;
import ru.t1.sukhorukova.tm.constant.CommandConst;
import ru.t1.sukhorukova.tm.controller.CommandController;
import ru.t1.sukhorukova.tm.controller.ProjectController;
import ru.t1.sukhorukova.tm.controller.TaskController;
import ru.t1.sukhorukova.tm.repository.CommandRepository;
import ru.t1.sukhorukova.tm.repository.ProjectRepository;
import ru.t1.sukhorukova.tm.repository.TaskRepository;
import ru.t1.sukhorukova.tm.service.CommandService;
import ru.t1.sukhorukova.tm.service.ProjectService;
import ru.t1.sukhorukova.tm.service.TaskService;
import ru.t1.sukhorukova.tm.util.TerminalUtil;

public final class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();
    private final ICommandService commandService = new CommandService(commandRepository);
    private final ICommandController commandController = new CommandController(commandService);

    private final IProjectRepository projectRepository = new ProjectRepository();
    private final IProjectService projectService = new ProjectService(projectRepository);
    private final IProjectController projectController = new ProjectController(projectService);

    private final ITaskRepository taskRepository = new TaskRepository();
    private final ITaskService taskService = new TaskService(taskRepository);
    private final ITaskController taskController = new TaskController(taskService);

    public void start(String[] args) {
        processArguments(args);
        System.out.println("** WELCOME TASK MANAGER **");
        while (!Thread.currentThread().isInterrupted()) {
            System.out.println("ENTER COMMAND:");
            processCommand(TerminalUtil.nextLine());
        }
    }

    private void processArguments(String[] args) {
        if (args == null || args.length == 0) return;
        processArgument(args[0]);
        System.exit(0);
    }

    private void processArgument(String command) {
        switch (command) {
            case ArgumentConst.CMD_VERSION:
                commandController.showVersion();
                break;
            case ArgumentConst.CMD_ABOUT:
                commandController.showAbout();
                break;
            case ArgumentConst.CMD_HELP:
                commandController.showHelp();
                break;
            case ArgumentConst.CMD_INFO:
                commandController.showInfo();
                break;
            case ArgumentConst.CMD_ARGUMENT:
                commandController.showArguments();
                break;
            case ArgumentConst.CMD_COMMAND:
                commandController.showCommands();
                break;
            default:
                commandController.showArgumentError();
                break;
        }
    }

    private void processCommand(String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case CommandConst.CMD_VERSION:
                commandController.showVersion();
                break;
            case CommandConst.CMD_ABOUT:
                commandController.showAbout();
                break;
            case CommandConst.CMD_HELP:
                commandController.showHelp();
                break;
            case CommandConst.CMD_INFO:
                commandController.showInfo();
                break;
            case CommandConst.CMD_EXIT:
                exit();
                break;
            case CommandConst.CMD_ARGUMENT:
                commandController.showArguments();
                break;
            case CommandConst.CMD_COMMAND:
                commandController.showCommands();
                break;
            case CommandConst.CMD_PROJECT_CREATE:
                projectController.createProject();
                break;
            case CommandConst.CMD_PROJECT_LIST:
                projectController.showProjects();
                break;
            case CommandConst.CMD_PROJECT_CLEAR:
                projectController.clearProjects();
                break;
            case CommandConst.CMD_TASK_CREATE:
                taskController.createTask();
                break;
            case CommandConst.CMD_TASK_LIST:
                taskController.showTasks();
                break;
            case CommandConst.CMD_TASK_CLEAR:
                taskController.clearTasks();
                break;
            default:
                commandController.showCommandError();
                break;
        }
    }

     private void exit() {
        System.exit(0);
    }

}
