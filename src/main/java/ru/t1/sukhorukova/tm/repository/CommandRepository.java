package ru.t1.sukhorukova.tm.repository;

import ru.t1.sukhorukova.tm.api.ICommandRepository;
import ru.t1.sukhorukova.tm.constant.ArgumentConst;
import ru.t1.sukhorukova.tm.constant.CommandConst;
import ru.t1.sukhorukova.tm.model.Command;

public final class CommandRepository implements ICommandRepository {

    public static final Command CMD_HELP = new Command(CommandConst.CMD_HELP, ArgumentConst.CMD_HELP, "Show help.");
    public static final Command CMD_VERSION = new Command(CommandConst.CMD_VERSION, ArgumentConst.CMD_VERSION, "Show version.");
    public static final Command CMD_ABOUT = new Command(CommandConst.CMD_ABOUT, ArgumentConst.CMD_ABOUT, "Show about developer.");
    public static final Command CMD_INFO = new Command(CommandConst.CMD_INFO, ArgumentConst.CMD_INFO, "Show system information.");
    public static final Command CMD_ARGUMENT = new Command(CommandConst.CMD_ARGUMENT, ArgumentConst.CMD_ARGUMENT, "Show argument list.");
    public static final Command CMD_COMMAND = new Command(CommandConst.CMD_COMMAND, ArgumentConst.CMD_COMMAND, "Show command list.");

    public static final Command CMD_PROJECT_CREATE = new Command(CommandConst.CMD_PROJECT_CREATE, null, "Create new project.");
    public static final Command CMD_PROJECT_LIST = new Command(CommandConst.CMD_PROJECT_LIST, null, "Show project list.");
    public static final Command CMD_PROJECT_CLEAR = new Command(CommandConst.CMD_PROJECT_CLEAR, null, "Remove all projects.");

    public static final Command CMD_TASK_CREATE = new Command(CommandConst.CMD_TASK_CREATE, null, "Create new task.");
    public static final Command CMD_TASK_LIST = new Command(CommandConst.CMD_TASK_LIST, null, "Show task list.");
    public static final Command CMD_TASK_CLEAR = new Command(CommandConst.CMD_TASK_CLEAR, null, "Remove all tasks.");

    public static final Command CMD_EXIT = new Command(CommandConst.CMD_EXIT, null, "Close application.");

    public static final Command[] TERMINAL_COMMANDS = {
            CMD_HELP, CMD_VERSION, CMD_ABOUT, CMD_INFO, CMD_ARGUMENT, CMD_COMMAND,
            CMD_PROJECT_CREATE, CMD_PROJECT_LIST, CMD_PROJECT_CLEAR,
            CMD_TASK_CREATE, CMD_TASK_LIST, CMD_TASK_CLEAR,
            CMD_EXIT
    };

    public Command[] getTerminalCommands() {
        return TERMINAL_COMMANDS;
    }

}
