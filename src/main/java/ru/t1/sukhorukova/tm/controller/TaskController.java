package ru.t1.sukhorukova.tm.controller;

import ru.t1.sukhorukova.tm.api.ITaskService;
import ru.t1.sukhorukova.tm.model.Task;
import ru.t1.sukhorukova.tm.util.TerminalUtil;

import java.util.List;

public final class TaskController implements ru.t1.sukhorukova.tm.api.ITaskController {

    private final ITaskService taskService;

    public TaskController(ITaskService taskService) {
        this.taskService = taskService;
    }

    @Override
    public void createTask() {
        System.out.println("[TASK CREATE]");

        System.out.println("Enter task name:");
        final String name = TerminalUtil.nextLine();

        System.out.println("Enter task description");
        final String description = TerminalUtil.nextLine();

        if (taskService.create(name, description) != null) System.out.println("[OK]");
        else System.out.println("[ERROR]");
    }

    @Override
    public void showTasks() {
        System.out.println("[TASK LIST]");
        int index = 1;
        final List<Task> tasks = taskService.findAll();
        for (final Task task: tasks) {
            if (task == null) continue;
            System.out.println(index++ + ". " + task.getName() + ": " + task.getDescription());
        }
        System.out.println("[OK]");
    }

    @Override
    public void clearTasks() {
        System.out.println("[TASK CLEAR]");
        taskService.clear();
        System.out.println("[OK]");
    }

}
